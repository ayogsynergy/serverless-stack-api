import handler from "./libs/handler-lib";
import dynamoDb from "./libs/dynamodb-lib";

export const main = handler(async (event, content) => {
    const data = JSON.parse(event.body);
    const params = {
        TableName: process.env.tableName,
        Key: {
            userid: event.requestContext.identity.cognitoIdentityId,
            noteid: event.pathParameters.id
        },
        UpdateExpression: "SET content = :content, attachement = :attachment",
        ExpressionAttributeValues: {
            ":attachment": data.attachment || null,
            ":content": data.content || null
        },
        // Specify how to return the values.
        // this returns all the attributes of the item after the update.
        ReturnValues: "ALL_NEW"
    };

    await dynamoDb.update(params);

    return { status: true };
});